#  Тестовый мониторинг

## Состав мониторинга: 
- [ ] Prometheus v2.42.0
- [ ] Grafana v9.3.2
- [ ] Zabbix-WEB v5.0.28
- [ ] Zabbix-Server v5.0.28
- [ ] PostgreSQL v14.6

## Для запуска необходимы:
- [ ] [Git](https://github.com/git-for-windows/git/releases/download/v2.39.0.windows.2/Git-2.39.0.2-64-bit.exe)
- [ ] [Docker](https://www.docker.com/products/docker-desktop/)
- [ ] [Docker-compose](https://docs.docker.com/compose/install/)

### Клонируем репозиторий:
```sh
git clone git@gitlab.com:devops-direction/test-monitoring.git
```

### Запуск контейнеров и создание сети на локальной машине:
```sh
cd test_monitoring
rm -rf DataBase/.gitkeep
docker-compose up -d
```

### Остановка контейнеров и удаление сети на локальной машине:
```sh
docker-compose down
```
> !!! Команды необходимо выполнять в директории с проетом !!!

## После запуска контейнеров, доступ к ним тут:
- [ ] [Zabbix](http://localhost) [Admin/zabbix]
- [ ] [Prometheus](http://localhost:9090) [admin/pswd-prometheus]
- [ ] [Alertmanager](http://localhost:9093) [admin/pswd-prometheus]
- [ ] [Grafana](http://localhost:8080) [admin/admin123]
